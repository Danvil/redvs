#include "mapper.hpp"
#include <Slimage/Slimage.hpp>
#include <Slimage/IO.hpp>
#include <Slimage/Gui.hpp>
#include <Candy.h>
#include <Danvil/Tools/FunctionCache.h>
#include <boost/lexical_cast.hpp>
#include <boost/progress.hpp>
#include <boost/format.hpp>
#include <GL/glew.h>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <memory>

using namespace std;

vector<vector<string>> LoadTSV(const string& fn)
{
	vector<vector<string>> lines;
	string line;
	fstream ifs(fn);
	if(!ifs.is_open()) {
		cerr << "ERROR loading file '" << fn << "'" << endl;
		return {};
	}
	while(getline(ifs, line)) {
		istringstream linestream(line);
		string token;
		vector<string> row;
		while(getline(linestream, token, '\t')) {
			row.push_back(token);
		}
		lines.push_back(row);
	}
	return lines;
}

Eigen::MatrixXf ImportMatrixFromImage3ub(const std::string& fn)
{
	slimage::Image3ub img = slimage::Load3ub(fn);
	Eigen::MatrixXf mat(img.width(), img.height());
	for(int i=0; i<mat.cols(); i++) {
		for(int j=0; j<mat.rows(); j++) {
			slimage::Pixel3ub px = img(j,i);
			mat(j,i) = 1.0f - static_cast<float>(px[0]) / 255.0f;
		}
	}
	return mat;
}

slimage::Image1f MatrixToImage(const Eigen::MatrixXf& m)
{
	slimage::Image1f img(m.rows(), m.cols());
	for(int i=0; i<m.cols(); i++) {
		for(int j=0; j<m.rows(); j++) {
			img(j,i) = m(j,i);
		}
	}
	return img;
}

Eigen::MatrixXf conv_x(const Eigen::MatrixXf& m, const Eigen::VectorXf& v)
{
	const int N = v.size();
	const int R = (N-1)/2;
	const int ROWS = m.rows();
	assert(N == 2*R+1);
	Eigen::MatrixXf r = Eigen::MatrixXf::Zero(m.rows(), m.cols());
	for(int i=0; i<m.cols(); i++) {
		for(int j=0; j<ROWS; j++) {
			float q;
			if(j < R) {
				const int cnt = R+j+1;
				auto b = v.block(R-j,0,cnt,1);
				q = m.block(0,i,cnt,1).cwiseProduct(b).sum() / b.sum();
				// if(i==10) {
				// 	cout << j << " " << cnt << " " << R-j << " " << 0 << endl;
				// 	cout << b.transpose() << endl;
				// }
			}
			else if(j+R >= ROWS) {
				const int cnt = R-j+ROWS;
				auto b = v.block(0,0,cnt,1);
				q = m.block(j-R,i,cnt,1).cwiseProduct(b).sum() / b.sum();
				// if(i==10) {
				// 	cout << j << " " << cnt << " " << 0 << " " << j-R << endl;
				// 	cout << b.transpose() << endl;
				// }
			}
			else {
				q = m.block(j-R,i,N,1).cwiseProduct(v).sum();
			}
			r(j,i) = q;
		}
	}
	return r;
}

Eigen::MatrixXf conv_y(const Eigen::MatrixXf& m, const Eigen::VectorXf& v)
{
	return conv_x(m.transpose(), v).transpose();
}

Eigen::MatrixXf conv2(const Eigen::MatrixXf& m, const Eigen::VectorXf& v)
{
	return conv_x(conv_y(m, v), v);
}

Eigen::VectorXf gauss_filter_kernel(int R, float sigma)
{
	const float scl = -0.5f/(sigma*sigma);
	Eigen::VectorXf v(2*R+1);
	for(int i=-R; i<=+R; i++) {
		float x = static_cast<float>(i);
		v[i+R] = exp(scl*x*x);
	}
	Eigen::VectorXf vn = v / v.sum();
	return vn;
}

vector<Scan> Load(const string& dir, const string& fn)
{
	vector<vector<string>> data = LoadTSV(dir + "/" + fn);
	vector<Scan> scans;
	for(const vector<string>& v : data) {
		if(v.size() != 3) {
			cerr << "ERROR loading file: not enough items in line!" << endl;
			return {};
		}
		if(v[0].front() == '#') {
			// comment
			continue;
		}
		assert(v.size() == 3);
		cout << "(" << v[0] << "," << v[1] << ") -> " << v[2] << " ";
		float px = boost::lexical_cast<float>(v[0]);
		float py = boost::lexical_cast<float>(v[1]);
		const string& fn = v[2];
		Scan u;
		u.pos = Eigen::Vector2f(px, py);
		u.scan[0] = ImportMatrixFromImage3ub(dir + "/" + fn);
		cout << u.scan[0].rows() << "x" << u.scan[0].cols() << endl;
		// compute scale images
		for(int i=0; i<MAX_SCAN_RN; i++) {
			int gscala = (1 << i); // scala (width [px] on image plane)
			float gsigma = static_cast<float>(gscala) * 0.25f; // sigma
			int gr = static_cast<int>(gsigma*2.0f + 0.5f); // radius
			u.scan[1+i] = conv2(u.scan[i], gauss_filter_kernel(gr, gsigma));
		}
		// for(int i=0; i<MAX_SCAN_RN+1; i++) {
		// 	slimage::gui::Show((boost::format("gauss %1%") % i).str(), MatrixToImage(u.scan[i]));
		// 	cout << "Gauss " << i << ": sum=" << u.scan[i].sum() << endl;
		// }
		scans.push_back(u);
	}
	return scans;
}

template<typename K>
inline bool in_interval(K x, K a, K b) {
	return a <= x && x < b;
}

template<typename K>
inline bool in_interval_01(K x) {
	return K(0) <= x && x < K(1);
}

constexpr float PI = 3.1415f;
constexpr int RETINA = 128;
constexpr float FOV = 56.0f; // degree
constexpr float RETINA_FOV_PX = 120.366f; // = (RETINA/2) / tan(FOV/2)
constexpr float RETINA_FOCAL = 1.88072647f; // = 1 / tan(FOV/2)
constexpr float R_MIN = 0.25f;
constexpr float SCAN_HEIGHT = 0.25f;
constexpr float ROOM_SIZE = 5.0f;
constexpr float CELL_SIZE = ROOM_SIZE / static_cast<float>(ROOM_N);
constexpr float RENDER_THRESHOLD = 0.8f;
constexpr float ERROR_POS = 0.01f;
constexpr float ERROR_ROT = 3.0f; // degree
constexpr float ERROR_ROT_FACTOR = 0.104816f;

// constexpr float ALPHA0 = 0.50f; // hallway
constexpr float ALPHA0 = 0.00f; // holodeck

template<typename F>
void vol_iterate(Volume<>& vol, F f)
{
	for(int z=0; z<vol.H; z++) {
		for(int y=0; y<vol.N; y++) {
			for(int x=0; x<vol.N; x++) {
				f(vol, x, y, z);
			}
		}
	}
}

void vol_normalize(Volume<float>& vol)
{
	float vmax = *max_element(vol.begin(), vol.end());
	cout << "vol_max=" << vmax << endl;
	for(float& v : vol) {
		v /= vmax;
	}
}

template<typename F, int N, int H>
Volume<Eigen::Vector3f,N,H> vol_positions(const Volume<F,N,H>& vol, const Eigen::Vector3f& offset, float cellsize)
{
	Volume<Eigen::Vector3f,N,H> pos;
	Eigen::Vector3f p;
	for(int z=0; z<H; z++) {
		p[2] = z;
		for(int y=0; y<N; y++) {
			p[1] = y;
			for(int x=0; x<N; x++) {
				p[0] = x;
				pos.at(x,y,z) = offset + cellsize * p;
//				cout << x << "," << y << "," << z << " -> " << pos.at(x,y,z).transpose() << endl;
			}
		}
	}
	return pos;
}

inline float exp_m(float x)
{
	Danvil::ProbabilityExpFunctionCache<float> cache;
	return cache(x);
//	return exp(-0.5f*x);
}

inline float Intensity(const Eigen::Vector3f& x, const Eigen::Vector3f& a, const Eigen::Vector3f& u)
{
	Eigen::Vector3f d = x - a;
	float dd = d.dot(d);
	if(dd < R_MIN*R_MIN) {
		return 0.0f;
	}
	float w = d.dot(u);
	if(w < 0) {
		return 0.0f;
	}
	float s = RETINA_FOV_PX*RETINA_FOV_PX*(1.0f - w*w / dd);
	return exp_m(s);
}

void room_add_ray(Room& room, const Eigen::Vector3f& a, const Eigen::Vector3f& u_, float I0)
{
	Eigen::Vector3f u = u_.normalized();
	const int N = room.N();
	const int H = room.H();
	for(int z=0; z<H; z++) {
		for(int y=0; y<N; y++) {
			for(int x=0; x<N; x++) {
				Eigen::Vector3f pos = room.pos.at(x,y,z);
				float I = Intensity(pos, a, u);
//				cout << x << "," << y << "," << z << " -> " << pos.transpose() << " -> " << I << endl;
				room.prop.at(x,y,z) += I0*I;
			}
		}
	}
}

void room_project_SLOOOOW(Room& room, const Scan& s)
{
	Eigen::Vector3f ray_a(
		0.5f + 0.5f * s.pos.x(),
		1.5f + 0.5f * s.pos.y(),
		SCAN_HEIGHT
	);
	int width = s.scan[0].rows();
	boost::progress_display progressbar(s.scan[0].size());
	for(int y=0; y<s.scan[0].cols(); y++) {
		float phi = FOV * static_cast<float>(y - RETINA/2);
		float ul = cos(phi);
		float uh = sin(phi);
		for(int x=0; x<width; x++, ++progressbar) {
			const float alpha = - 2.0f*PI*static_cast<float>(x)/static_cast<float>(width);
			const float nx = cos(alpha);
			const float ny = sin(alpha);
			Eigen::Vector3f ray_u(ul*nx, ul*ny, uh);
			float I0 = s.scan[0](x,y);
			if(I0 > 0.5f) {
				//std::cout << nx << "," << ny << " -> " << I0 << std::endl;
				room_add_ray(room, ray_a, ray_u, I0);
			}
		}
	}
}

inline float clamp_01(float x) {
	while(x < 0.0f) x += 1.0f;
	while(x >= 1.0f) x -= 1.0f;
	return x;
}

inline void project_onto_scan_0(const Eigen::Vector3f& p, float& fx, float& fy)
{
	const float x = p[0];
	const float y = p[1];
	const float z = p[2];
	fx = clamp_01(ALPHA0-(0.5f/PI)*atan2(y,x)); // range is 0..1
	fy = 0.5f - 0.5f*RETINA_FOCAL*z/sqrt(x*x+y*y); // range is 0..1
}

inline void project_onto_scan(const Eigen::Vector3f& p, const Eigen::Vector3f& a, float& fx, float& fy)
{
	project_onto_scan_0(p - a, fx, fy);
}

inline float scala_from_dist(float dist)
{
	return RETINA_FOV_PX*( (CELL_SIZE + ERROR_POS)/dist + ERROR_ROT_FACTOR );
}

inline float scala_lookup(int ix, int iy, float dist, const Eigen::MatrixXf* scans)
{
	float scalaf = scala_from_dist(dist);
	float level = max<float>(0.0f, log2(scalaf));
	int s = min<int>(MAX_SCAN_RN, static_cast<int>(level));
	int t = min<int>(MAX_SCAN_RN, s + 1);
	float p = level - static_cast<float>(s);
	// return scans[0](ix,iy);
	// return scans[s](ix,iy);
	return interpolateLinear(scans[s](ix,iy), scans[t](ix,iy), p);
}

void project_voxel_onto_scan(const Room& room, const Scan& s, int x, int y, int z, int& px, int& py, float& ps)
{
	Eigen::Vector3f scan_pos(s.pos.x(), s.pos.y(), SCAN_HEIGHT);
	const Eigen::Vector3f& pos = room.pos.at(x,y,z);
	float fx, fy;
	project_onto_scan(pos, scan_pos, fx, fy);
	const int rows = s.scan[0].rows();
	const int cols = s.scan[0].cols();
	px = static_cast<int>(static_cast<float>(rows)*fx + 0.5f);
	py = static_cast<int>(static_cast<float>(cols)*fy + 0.5f);
	if(in_interval(px,0,rows) && in_interval(py,0,cols)) {
		float dist = (pos - scan_pos).norm();
		ps = scala_from_dist(dist);
	}
	else {
		ps = 0.0f;
	}
}

void room_project(Room& room, const Scan& s)
{
	const int N = room.N();
	const int H = room.H();
	const int rows = s.scan[0].rows();
	const int cols = s.scan[0].cols();
	Eigen::Vector3f scan_pos(s.pos.x(), s.pos.y(), SCAN_HEIGHT);
	for(int z=0; z<H; z++) {
		for(int y=0; y<N; y++) {
			for(int x=0; x<N; x++) {
				const Eigen::Vector3f& pos = room.pos.at(x,y,z);
				float fx, fy;
				project_onto_scan(pos, scan_pos, fx, fy);
//				cout << room.pos.at(x,y,z).transpose() << " " << fx << " " << fy << endl;
				int ix = static_cast<int>(static_cast<float>(rows)*fx + 0.5f);
				int iy = static_cast<int>(static_cast<float>(cols)*fy + 0.5f);
				if(in_interval(ix,0,rows) && in_interval(iy,0,cols)) {
					// compute scala
					float dist = (pos - scan_pos).norm();
					// get intensity
					room.prop.at(x,y,z) += scala_lookup(ix, iy, dist, s.scan);
				}
			}
		}
	}
}

Room CreateRoom(const vector<Scan>& scans)
{
	Room room;

	fill(room.prop.begin(), room.prop.end(), 0.0f);
	room.pos = vol_positions(room.prop, Eigen::Vector3f(-1.0f,-1.0f,0.0f), CELL_SIZE);

	boost::progress_display progressbar(scans.size());
	for(const Scan& s : scans) {
		room_project(room, s);
		++progressbar;
	}

	// room_project(room, scans.front());

	// for(int i=0; i<4; i++) {
	// 	room_project(room, scans[i]);
	// }

	vol_normalize(room.prop);

	return room;
}

inline void opengl_vertex3f(const Eigen::Vector3f& v) {
	glVertex3f(v.x(), v.y(), v.z());
}

inline void render_particle(const Eigen::Vector3f& pos, float r, float q, const Eigen::Vector3f& right, const Eigen::Vector3f& up)
{
//	glColor4f(q,q,q,1.0f);
	glColor4f(1.0f, 1.0f, 1.0f, 0.2f*q);
//	glColor4f(q, 0, 1-q, q);
	opengl_vertex3f(pos - r*right - r*up);
	opengl_vertex3f(pos + r*right - r*up);
	opengl_vertex3f(pos + r*right + r*up);
	opengl_vertex3f(pos - r*right + r*up);
}

void RenderRoom(const boost::shared_ptr<Candy::Engine>& candy_engine, const Room& room)
{
	auto camera = candy_engine->getView()->getCamera();
	Eigen::Vector3f right(camera->getRight().x, camera->getRight().y, camera->getRight().z);
	Eigen::Vector3f up(camera->getUp().x, camera->getUp().y, camera->getUp().z);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	//glBlendFunc(GL_ONE, GL_ONE);
	glEnable(GL_BLEND);
	glDepthMask(GL_FALSE);
	glBegin(GL_QUADS);

	const int N = room.N();
	const int H = room.H();
	for(int z=0; z<H; z++) {
		for(int y=0; y<N; y++) {
			for(int x=0; x<N; x++) {
				float I = room.prop.at(x,y,z);
				if(I > RENDER_THRESHOLD) {
					render_particle(room.pos.at(x,y,z), 0.6f*CELL_SIZE, I, right, up);
				}
			}
		}
	}

	glEnd();

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);

}
