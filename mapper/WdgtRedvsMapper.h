#ifndef WdgtRedvsMapper_H
#define WdgtRedvsMapper_H

#include "mapper.hpp"
#include <Candy/System/GLSystemQtWindow.h>
#include <Candy.h>
#include <QtGui/QWidget>
#include <QtGui/QLabel>
#include <QtGui/QScrollArea>
#include <QtCore/QTimer>
#include "ui_WdgtRedvsMapper.h"
#include <string>

class WdgtRedvsMapper 
: public QWidget
{
    Q_OBJECT

public:
	WdgtRedvsMapper(const std::string& dir, const std::string& fn, QWidget *parent = 0);
	virtual ~WdgtRedvsMapper();

public Q_SLOTS:
	void OnUpdateParameters();

private:
	void Render();

private:
	boost::shared_ptr<Candy::View> view_;
	boost::shared_ptr<Candy::Scene> scene_;
	boost::shared_ptr<Candy::Engine> engine_;
	Candy::GLSystemQtWindow* gl_wdgt_;

	QLabel* imageLabel;
	QScrollArea* scrollArea;

	QLabel* imageLabelDensity;
	QScrollArea* scrollAreaDensity;

	std::vector<Scan> scans;
	Room the_room;

private:
    Ui::WdgtRedvsMapper ui;
};

#endif
