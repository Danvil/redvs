#include "WdgtRedvsMapper.h"
#include <QtGui/QImage>
#include <QtGui/QPainter>
#include <iostream>

WdgtRedvsMapper::WdgtRedvsMapper(const std::string& dir, const std::string& fn, QWidget *parent)
: QWidget(parent)
{
	ui.setupUi(this);

	std::cout << "Creating OpenGL Widget ..." << std::endl;

	view_ = Candy::View::FactorDefaultPerspectiveView();
	scene_ = view_->getScene();

	// boost::shared_ptr<Candy::DirectionalLight> light1(new Candy::DirectionalLight(Danvil::ctLinAlg::Vec3f(+1.0f, +1.0f, -1.0f)));
	// light1->setDiffuse(Danvil::Colorf(1.0f, 1.0f, 1.0f));
	// scene_->addLight(light1);
	// boost::shared_ptr<Candy::DirectionalLight> light2(new Candy::DirectionalLight(Danvil::ctLinAlg::Vec3f(-1.0f, -1.0f, -1.0f)));
	// light2->setDiffuse(Danvil::Colorf(1.0f, 1.0f, 1.0f));
	// scene_->addLight(light2);

	engine_.reset(new Candy::Engine(view_));
//	engine_->setClearColor(Danvil::Color::Grey);
	engine_->setClearColor(Danvil::Color::Black);

	scene_->setShowCoordinateCross(true);

	gl_wdgt_ = new Candy::GLSystemQtWindow(0, engine_);
	gl_wdgt_->show();

	boost::shared_ptr<Candy::IRenderable> dasp_renderling(
			new Candy::ObjectRenderling([this](){ Render(); }));
	scene_->addItem(dasp_renderling);

	std::cout << "Loading data ..." << std::endl;
	scans = Load(dir, fn);
	std::cout << "Scanes loaded: " << scans.size() << std::endl;

	std::cout << "Integrating ..." << std::flush;
	the_room = CreateRoom(scans);
	std::cout << "Finished." << std::endl;

	// show images
	imageLabel = new QLabel;
	imageLabel->setBackgroundRole(QPalette::Base);
	imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	imageLabel->setScaledContents(true);
	scrollArea = new QScrollArea;
	scrollArea->setWindowTitle("input images");
	scrollArea->setBackgroundRole(QPalette::Dark);
	scrollArea->setWidgetResizable(false);
	scrollArea->setWidget(imageLabel);
	scrollArea->show();

	// show images
	imageLabelDensity = new QLabel;
	imageLabelDensity->setBackgroundRole(QPalette::Base);
	imageLabelDensity->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	imageLabelDensity->setScaledContents(true);
	scrollAreaDensity = new QScrollArea;
	scrollAreaDensity->setWindowTitle("input images");
	scrollAreaDensity->setBackgroundRole(QPalette::Dark);
	scrollAreaDensity->setWidgetResizable(false);
	scrollAreaDensity->setWidget(imageLabelDensity);
	scrollAreaDensity->show();

	ui.spinBoxRedvsViewerScalaLevel->setMaximum(MAX_SCAN_RN);
	ui.spinBoxRedvsViewerScalaLevel->setValue(0);
	connect(ui.spinBoxRedvsViewerScalaLevel, SIGNAL(valueChanged(int)), this, SLOT(OnUpdateParameters()));

	ui.horizontalSliderDensitySliceHeight->setMaximum(the_room.prop.H-1);
	ui.horizontalSliderDensitySliceHeight->setValue(the_room.prop.H/2);
	connect(ui.horizontalSliderDensitySliceHeight, SIGNAL(valueChanged(int)), this, SLOT(OnUpdateParameters()));

	ui.horizontalSliderTesterPositionX->setMaximum(the_room.prop.N-1);
	ui.horizontalSliderTesterPositionX->setValue(the_room.prop.N/2);
	connect(ui.horizontalSliderTesterPositionX, SIGNAL(valueChanged(int)), this, SLOT(OnUpdateParameters()));

	ui.horizontalSliderTesterPositionY->setMaximum(the_room.prop.N-1);
	ui.horizontalSliderTesterPositionY->setValue(the_room.prop.N/2);
	connect(ui.horizontalSliderTesterPositionY, SIGNAL(valueChanged(int)), this, SLOT(OnUpdateParameters()));

	OnUpdateParameters();
}

WdgtRedvsMapper::~WdgtRedvsMapper()
{
	gl_wdgt_->hide();
	delete gl_wdgt_;
}

QRgb ColorPaletteDensity(float v)
{
	v *= 2.0f;
	if(v <= 1.0f) {
		unsigned char g = static_cast<int>(255.0f*v);
		// blue -> red
		return qRgb(g, 0, 255-g);
	}
	else {
		v -= 1.0f;
		unsigned char g = static_cast<int>(255.0f*v);
		// red -> yellow
		return qRgb(255, g, 0);
	}
}

QRgb ColorPaletteDensityPlusGreen(float v)
{
	v *= 3.0f;
	if(v <= 1.0f) {
		unsigned char g = static_cast<int>(255.0f*v);
		// blue -> red
		return qRgb(g, 0, 255-g);
	}
	if(v <= 2.0f) {
		v -= 1.0f;
		unsigned char g = static_cast<int>(255.0f*v);
		// red -> yellow
		return qRgb(255, g, 0);
	}
	else {
		v -= 2.0f;
		unsigned char g = static_cast<int>(255.0f*v);
		// yellow -> green
		return qRgb(255-g, 255, 0);
	}
}

QRgb ColorPaletteGray(float v)
{
	unsigned char g = 255-static_cast<int>(255.0f*v);
	return qRgb(g,g,g);
}

template<typename ColorFunc>
QImage MatrixToImage(const Eigen::MatrixXf& m, ColorFunc cf)
{
	QImage img(m.rows(), m.cols(), QImage::Format_ARGB32);
	for(int i=0; i<m.cols(); i++) {
		for(int j=0; j<m.rows(); j++) {
			img.setPixel(j,i,cf(m(j,i)));
		}
	}
	return img;
}

void PlotHightlight(QImage& img, int cx, int cy, float rf)
{
	const int r = static_cast<int>(rf + 0.5f);
	const int r2 = static_cast<int>(rf*rf + 0.5f);
	// QPainter pnt(&img);
	// pnt.fillRect(cx-r, cy-r, 2*r+1, 2*r+1, QColor(qRgb(255,0,0)));
	const int x1 = std::max<int>(cx-r, 0);
	const int x2 = std::min<int>(cx+r, img.width()-1);
	const int y1 = std::max<int>(cy-r, 0);
	const int y2 = std::min<int>(cy+r, img.height()-1);
	for(int y=y1; y<=y2; y++) {
		for(int x=x1; x<=x2; x++) {
			int dx = x - cx;
			int dy = y - cy;
			if(dx*dx + dy*dy < r2) {
				// ASSUME col is grayscale
				unsigned char g = qRed(img.pixel(x,y));
				img.setPixel(x, y, qRgb(255-g,g,0));
			}
		}
	}
}

QImage PlotScans(const Room& room, const std::vector<Scan>& scans, int level, int tx, int ty, int tz)
{
	int width = scans.front().scan[level].rows();
	int height = scans.front().scan[level].cols();
	for(const Scan& s : scans) {
		if(s.scan[level].rows() != width || s.scan[level].cols() != height) {
			std::cerr << "Scan have different size!" << std::endl;
		}
	}
	QImage img(width, height*scans.size(), QImage::Format_ARGB32);
	for(int i=0; i<scans.size(); i++) {
		for(int y=0; y<height; y++) {
			for(int x=0; x<width; x++) {
				float v = scans[i].scan[level](x,y);
				img.setPixel(x, i*height+y, ColorPaletteGray(v));
			}
		}
		// tester
		int px, py;
		float ps;
		project_voxel_onto_scan(room, scans[i], tx, ty, tz, px, py, ps);
		std::cout << i << ": (" << tx << "," << ty << "," << tz << ") -> (" << px << "," << py << ") / " << ps << std::endl;
		if(0 <= px && px < width && 0 <= py && py < height) {
			PlotHightlight(img, px, i*height+py, ps);
		}
	}
	return img;
}

QImage PlotDensity(const Room& room, float h, int tx, int ty)
{
	int width = room.prop.N;
	int height = room.prop.N;
	int hi = std::min<int>(room.prop.H-1, std::max<int>(0, static_cast<int>(h)));

	Eigen::MatrixXf mat(width, height);
	for(int y=0; y<height; y++) {
		for(int x=0; x<width; x++) {
			mat(x,y) = room.prop.at(x,y,hi);
		}
	}

	QImage img = MatrixToImage(mat, [](float v) { return ColorPaletteDensityPlusGreen(v); } );

	if(0 <= tx && tx < width && 0 <= ty && ty < height) {
		QPainter pnt(&img);
		pnt.fillRect(tx-1, ty-1, 3, 3, QColor(qRgb(255,255,255)));
		// std::cout << room.prop.at(tx, ty, hi) << std::endl;
	}

	return img.mirrored();
}

void WdgtRedvsMapper::OnUpdateParameters()
{
	int level = ui.spinBoxRedvsViewerScalaLevel->value();
	int tx = ui.horizontalSliderTesterPositionX->value();
	int ty = ui.horizontalSliderTesterPositionY->value();
	int h = ui.horizontalSliderDensitySliceHeight->value();
	{
		imageLabel->setPixmap(QPixmap::fromImage(PlotScans(the_room,scans,level,tx,ty,h)));
		imageLabel->adjustSize();
	}
	{
		imageLabelDensity->setPixmap(QPixmap::fromImage(PlotDensity(the_room,h,tx,ty)));
		imageLabelDensity->adjustSize();
	}
}

void WdgtRedvsMapper::Render()
{
	RenderRoom(engine_, the_room);
}
