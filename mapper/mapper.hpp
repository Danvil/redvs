#include <Eigen/Dense>
#include <boost/shared_ptr.hpp>
#include <vector>
#include <string>

namespace Candy { class Engine; }

constexpr int MAX_SCAN_RN = 5;
constexpr int ROOM_N = 256;

struct Scan
{
	Eigen::Vector2f pos;
	Eigen::MatrixXf scan[MAX_SCAN_RN+1];
};

std::vector<Scan> Load(const std::string& dir, const std::string& fn);

inline float interpolateLinear(float a, float b, float p) {
	return (1.0f - p)*a + p*b;
}

template<typename F=float, int N_=ROOM_N, int H_=N_/4, typename C=int>
struct Volume
{
	static constexpr int N = N_;
	static constexpr int H = H_;
	static constexpr int LEN = N*N*H;

	typedef std::vector<F> container_t;
	typedef typename container_t::iterator it_t;
	typedef typename container_t::const_iterator cit_t;

	container_t v;

	Volume() {
		v.resize(LEN);
	}

	container_t& data() { return v; }
	const container_t& data() const { return v; }

	it_t begin() { return v.begin(); }
	cit_t begin() const { return v.begin(); }
	it_t end() { return v.end(); }
	cit_t end() const { return v.end(); }

	C index(C x, C y, C z) const { return x + N*(y + z*N); }

	F at(C x, C y, C z) const { return v[index(x,y,z)]; }
	F& at(C x, C y, C z) { return v[index(x,y,z)]; }

	F atSmoothZ(C x, C y, F z) const {
		// ASSERT(0 <= z && z <= H-1)
		const int s = static_cast<int>(z);
		const int t = std::max<int>(s+1, H-1);
		return interpolateLinear(at(x,y,s), at(x,y,t), z - static_cast<float>(s));
	}

};

struct Room
{
	Volume<float> prop;
	Volume<Eigen::Vector3f> pos;

	int N() const { return prop.N; }
	int H() const { return prop.H; }

};

Room CreateRoom(const std::vector<Scan>& scans);

void RenderRoom(const boost::shared_ptr<Candy::Engine>& candy_engine, const Room& room);

void project_voxel_onto_scan(const Room& room, const Scan& s, int x, int y, int z, int& px, int& py, float& ps);
