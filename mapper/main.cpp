
#include "WdgtRedvsMapper.h"
#include <QApplication>
#include <boost/program_options.hpp>

int main(int argc, char** argv)
{
	std::string p_dir = "";
	std::string p_fn = "";

	namespace po = boost::program_options;
	// Declare the supported options.
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("dir", po::value(&p_dir), "database directory")
		("fn", po::value(&p_fn), "database file")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if(vm.count("help")) {
		std::cout << desc << std::endl;
		return 1;
	}

	QApplication a(argc, argv);
	WdgtRedvsMapper w(p_dir, p_fn);
	w.show();
	return a.exec();
}
