
#include <Edvs/edvs.h>
#include <Edvs/edvs_impl.h>
#include <chrono>
#include <thread>
#include <string>
#include <iostream>

void send_and_wait(int sock, const std::string& cmd, int dt)
{
	std::string cmdn = cmd;
	cmdn += '\n';
	std::cout << "Sending: " << cmd << std::endl;
	edvs_net_write(sock, (char*)cmdn.data(), cmdn.length());
	std::cout << "Waiting for " << dt << " ms" << std::endl;
	std::this_thread::sleep_for(std::chrono::milliseconds(dt));
}

int main(int argc, char** argv)
{

	int sock = edvs_net_open("192.168.201.62", 56000);
	std::cout << sock << std::endl;

	send_and_wait(sock, "!H+", 100);
	send_and_wait(sock, "!H1=0", 1500);
	send_and_wait(sock, "!H1=800", 1500);
	send_and_wait(sock, "!H1=-800", 1500);
	send_and_wait(sock, "!H1=0", 1500);
	send_and_wait(sock, "!H-", 100);

	edvs_net_close(sock);

	return 1;
}
