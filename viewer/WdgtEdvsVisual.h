#ifndef WDGTEDVSVISUAL_H
#define WDGTEDVSVISUAL_H

#include "Scan.hpp"
#include <QtGui/QWidget>
#include <QtCore/QTimer>
#include "ui_WdgtEdvsVisual.h"
#include <Edvs/EventStream.hpp>
#include <Edvs/EventCapture.hpp>
#include <Eigen/Dense>
#include <boost/thread.hpp>
#include <vector>

class EdvsVisual : public QWidget
{
    Q_OBJECT

public:
	EdvsVisual(const Edvs::EventStream& dh, QWidget *parent = 0);
	~EdvsVisual();

	void OnEvent(const std::vector<Edvs::Event>& events);
	
	void OnSpecial(const std::vector<edvs_special_t>& special);

public Q_SLOTS:
	void Update();
	void Display();

	void OnChangeCamera();

	void OnSave();

private:
	Edvs::EventStream edvs_event_stream_;
	Edvs::EventCapture edvs_event_capture_;
	std::vector<Edvs::Event> events_;
	std::vector<edvs_special_t> special_;
	boost::mutex events_mtx_;
	QTimer timer_update_;
	QTimer timer_display_;
	QImage image_;

	float servo_angle_;

	Edvs::Redvs::Scan scan_;
	QImage scan_image_;

private:
    Ui::EdvsVisualClass ui;
};

#endif
