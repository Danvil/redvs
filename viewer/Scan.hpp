#ifndef EDVS_REDVS_SCAN
#define EDVS_REDVS_SCAN

#include <Eigen/Dense>
#include <Edvs/Event.hpp>

namespace Edvs
{
	namespace Redvs
	{

		struct Scan
		{
			static constexpr int RES = 128;
			static constexpr int CENTRE = RES/2;
			static constexpr int NORM_SAMPLES = 1;
			static constexpr float NORM_SCALE = 0.07f;

			Scan() {
				setCamera(60.0f, 0.0f, 0.0f);
			}

			void setCamera(float fov, float k1, float k2) {
				fov_ = fov;
				k1_ = k1;
				k2_ = k2;
				angle_max_ = +180.0f;
				angle_min_ = -180.0f;
				int m = static_cast<int>((angle_max_ - angle_min_) / fov_ * static_cast<float>(RES) + 0.5f);
				data_ = Eigen::MatrixXf::Zero(m, RES);
				norm_ = Eigen::VectorXf::Zero(m/NORM_SAMPLES);
			}

			void undistortAndCenter(int ix, int iy, float& x, float& y) const {
				ix -= CENTRE;
				iy -= CENTRE;
				const int r2 = ix*ix + iy*iy;
				const float r = std::sqrt(static_cast<float>(r2));
				const float L = 1.0f + k1_*r + k2_*r2;
				x = L * static_cast<float>(ix);
				y = L * static_cast<float>(iy);
			}

			template<typename Q>
			static Q clamp(Q x, Q n) {
				while(x < 0) x += n;
				while(x >= n) x -= n;
				return x;
			}

			void add(const Event& event, float angle) {
				float x, y;
				undistortAndCenter(event.x, event.y, x, y);
				float a = angle + fov_ * x / static_cast<float>(RES);
				float q = (a - angle_min_) / fov_ * static_cast<float>(RES);
				int fx = clamp<int>(static_cast<int>(q + 0.5f), data_.rows());
				int fy = CENTRE + static_cast<int>(y + 0.5f);
				if(0 <= fy && fy < data_.cols()) {
					data_(fx,fy) += 1.0f;
				}
				//std::cout << event.x << "," << event.y << "," << angle << " -> " << w << std::endl;
			}

			void addNorm(float angle) {
				// float c = getCenterCoordF(angle)/static_cast<float>(NORM_SAMPLES);
				// float u = clamp<float>(c, norm_.rows());
				// int q = static_cast<int>(u);
				// float r = u - static_cast<float>(q);
				// norm_[q] += 1.0f - r;
				// norm_[(q+1) % norm_.rows()] += r;
				int q = getCenterCoord(angle)/NORM_SAMPLES;
				int w = static_cast<int>(0.5f * static_cast<float>(RES) + 0.5f)/NORM_SAMPLES;
				int n = norm_.rows();
				for(int x=q-w; x<=q+w; x++) {
					norm_[clamp<int>(x, n)] += 1.0f;
				}
			}

			float getCenterCoordF(float angle) const {
				return (angle - angle_min_) / fov_ * static_cast<float>(RES);
			}

			int getCenterCoord(float angle) const {
				float q = getCenterCoordF(angle);
				return static_cast<int>(q + 0.5f);
			}

			float atRaw(int x, int y) const {
				return data_(x,y);
			}

			float at(int x, int y) const {
				float p = data_(x,y);
				float q = NORM_SCALE*norm_[x/NORM_SAMPLES];
				if(q == 0.0f) {
					return 0.0f;
				}
				else {
					return p / q;
				}
			}

			float normMin() const {
				return NORM_SCALE*norm_.minCoeff();
			}

			float fov_;
			float k1_, k2_;
			float angle_min_;
			float angle_max_;

			Eigen::MatrixXf data_;

			Eigen::VectorXf norm_;

		};

	}

}

#endif
