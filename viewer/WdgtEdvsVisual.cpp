#include "WdgtEdvsVisual.h"
#include <QtGui/QFileDialog>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/format.hpp>
#include <iostream>
#include <time.h>


const unsigned int RetinaSize = 128;
const int cDecay = 20;
const int cDisplaySize = 128;
const int cUpdateInterval = 10;
const int cDisplayInterval = 25;

// blue/yellow color scheme
// const QRgb cColorMid = qRgb(0, 0, 0);
// const QRgb cColorOn = qRgb(255, 255, 0);
// const QRgb cColorOff = qRgb(0, 0, 255);

// black/white color scheme
const QRgb cColorMid = qRgb(128, 128, 128);
const QRgb cColorOn = qRgb(255, 255, 255);
const QRgb cColorOff = qRgb(0, 0, 0);

EdvsVisual::EdvsVisual(const Edvs::EventStream& dh, QWidget *parent)
    : QWidget(parent)
{
	ui.setupUi(this);

	image_ = QImage(RetinaSize, RetinaSize, QImage::Format_RGB32);

	connect(ui.doubleSpinBoxFOV, SIGNAL(valueChanged(double)), this, SLOT(OnChangeCamera()));
	connect(ui.doubleSpinBoxKappa1, SIGNAL(valueChanged(double)), this, SLOT(OnChangeCamera()));
	connect(ui.doubleSpinBoxKappa2, SIGNAL(valueChanged(double)), this, SLOT(OnChangeCamera()));
	connect(ui.spinBoxTimeOffset, SIGNAL(valueChanged(int)), this, SLOT(OnChangeCamera()));
	connect(ui.pushButtonSave, SIGNAL(clicked()), this, SLOT(OnSave()));

	OnChangeCamera();

	connect(&timer_update_, SIGNAL(timeout()), this, SLOT(Update()));
	timer_update_.setInterval(cUpdateInterval);
	timer_update_.start();

	connect(&timer_display_, SIGNAL(timeout()), this, SLOT(Display()));
	timer_display_.setInterval(cDisplayInterval);
	timer_display_.start();

	// start capture
	edvs_event_stream_ = dh;
	edvs_event_capture_ = Edvs::EventCapture(edvs_event_stream_,
		boost::bind(&EdvsVisual::OnEvent, this, _1),
		boost::bind(&EdvsVisual::OnSpecial, this, _1)
	);

	// write motor // HACK
	// edvs_event_stream_.write("!H+");
	// edvs_event_stream_.write("!HS1=4");
	edvs_event_stream_.write("!x+");
	edvs_event_stream_.write("!xv=50");
	edvs_event_stream_.write("!xa+");
}

EdvsVisual::~EdvsVisual()
{
	// write motor // HACK
	// edvs_event_stream_.write("!H-");
	edvs_event_stream_.write("!xa-");
	edvs_event_stream_.write("!x-");
}

void EdvsVisual::OnEvent(const std::vector<Edvs::Event>& newevents)
{
	// just store events
	// protect common vector with a mutex to avoid race conditions
	boost::interprocess::scoped_lock<boost::mutex> lock(events_mtx_);
	//events_.insert(events_.end(), newevents.begin(), newevents.end());

	for(Edvs::Event e : newevents) {

		// HACK why do we have to transform the event?
		// HACK is the sensor rotated?
		std::swap(e.x, e.y);
		e.x = 127 - e.x;

		events_.push_back(e);
	}

	// print time information
	if(!newevents.empty()) {
		static uint64_t last_time = 0;
		uint64_t current_time = newevents.back().t;
		if(current_time >= last_time + 1000000) {
			std::cout << "time=" << static_cast<float>(current_time)/1000000.0f << std::endl;
			std::cout << "angle=" << servo_angle_ << std::endl;
			last_time = current_time;
		}
	}
}

float compute_servo_angle(const edvs_special_t& q)
{
	unsigned int servo_pos =
		(static_cast<unsigned int>(q.data[0]) << 8)
		+ static_cast<unsigned int>(q.data[1]);
	return -(static_cast<float>(servo_pos) - 2047.5f) / 2047.5f * 180.0f;
}

void EdvsVisual::OnSpecial(const std::vector<edvs_special_t>& newspecial)
{
	boost::interprocess::scoped_lock<boost::mutex> lock(events_mtx_);
	special_.insert(special_.end(), newspecial.begin(), newspecial.end());
	for(const edvs_special_t& s : newspecial) {
		float angle = compute_servo_angle(s);
		scan_.addNorm(angle);
	}
	if(newspecial.size() > 0) {
		edvs_special_t q = newspecial.back();
		servo_angle_ = compute_servo_angle(q);
		// std::cout << "Num special: " << newspecial.size() << std::endl;
		// for(edvs_special_t q : newspecial) {
		// 	std::cout << q.n << ": ";
		// 	for(int i=0; i<q.n; i++) {
		// 		std::cout << (unsigned int)(q.data[i]) << " ";
		// 	}
		// 	std::cout << std::endl;
		// 	// unsigned w1 = q.data[0] + 256*q.data[1];
		// 	// unsigned w2 = q.data[1] + 256*q.data[0];
		// 	// std::cout << w2 << " " << w1 << std::endl;
		// }
	}
}

int DecayComponent(int current, int target, int decay)
{
	if(current-decay >= target) {
		return current - decay;
	}
	if(current+decay <= target) {
		return current + decay;
	}
	return target;
}

QRgb DecayColor(QRgb color, QRgb target, int decay)
{
	return qRgb(
		DecayComponent(qRed(color), qRed(target), cDecay),
		DecayComponent(qGreen(color), qGreen(target), cDecay),
		DecayComponent(qBlue(color), qBlue(target), cDecay)
	);
}

int time_offset = 0;

void EdvsVisual::OnChangeCamera()
{
	scan_.setCamera(
		ui.doubleSpinBoxFOV->value(),
		ui.doubleSpinBoxKappa1->value(),
		ui.doubleSpinBoxKappa2->value());
	time_offset = ui.spinBoxTimeOffset->value();
}

void EdvsVisual::OnSave()
{
	QString fn = QFileDialog::getSaveFileName();
	scan_image_.save(fn);
}

float interpolate_angle(float a1, uint64_t t1, float a2, uint64_t t2, uint64_t t)
{
	// static int num = 0;
	assert(t1 <= t && t <= t2);
	float p = static_cast<float>(t - t1) / static_cast<float>(t2 - t1);
	float a = (1.0f - p)*a1 + p*a2;
	// if(num % 300 == 0) {
	// 	std::cout << t1 << ":" << a1 << " / " << t2 << ":" << a2 << " / " << t << ":" << a << std::endl;
	// }
	// num ++;
	return a;
}

void EdvsVisual::Update()
{
	static std::vector<Edvs::Event> events_last;
	std::vector<Edvs::Event> events;
	std::vector<edvs_special_t> special;
	{
		boost::interprocess::scoped_lock<boost::mutex> lock(events_mtx_);
		events = events_;
		events_.clear();
		special = special_;
	}

	// write events
	{
		for(Edvs::Event& e : events) {
			image_.setPixel(e.x, e.y, e.parity ? cColorOn : cColorOff);
		}
	}

	// write scan
	{
		if(special.size() != 0) {
			events_last.insert(events_last.begin(), events.begin(), events.end());
			auto jt = events_last.begin();
			for(; jt!=events_last.end(); jt++) {
				const Edvs::Event& e = *jt;
				uint64_t angle_time = e.t - time_offset;
				auto it = std::lower_bound(special.begin(), special.end(), angle_time,
					[](const edvs_special_t& s, uint64_t t) {
						return s.t < t;
					});
				if(it == special.end()) {
					// did not get corresponding special event
					break;
				}

				if(it == special.begin()) {
					continue;
				}
				float angle2 = compute_servo_angle(*it);
				uint64_t time2 = it->t;
				--it;
				float angle1 = compute_servo_angle(*it);
				uint64_t time1 = it->t;

				float angle = interpolate_angle(angle1, time1, angle2, time2, angle_time);
				scan_.add(e, angle);
			}
			events_last.erase(events_last.begin(), jt);
		}
	}

}

void EdvsVisual::Display()
{
	{
		// apply decay
		// unsigned int* bits = (unsigned int*)image_.bits();
		// const unsigned int N = image_.height() * image_.width();
		// for(int i=0; i<N; i++, bits++) {
		// 	*bits = DecayColor(*bits, cColorMid, cDisplayInterval*cDecay);
		// }
		int h = image_.height();
		int w = image_.width();
		for(int y=0; y<h; y++) {
			for(int x=0; x<w; x++) {
				image_.setPixel(x,y, 
					DecayColor(image_.pixel(x,y),
						cColorMid, cDisplayInterval*cDecay));
			}
		}
		// rescale so that we see more :) and display
		ui.label->setPixmap(QPixmap::fromImage(image_.scaled(cDisplaySize, cDisplaySize)));
	}

	// display scan
	{
		int rows = scan_.data_.rows();
		int cols = scan_.data_.cols();

		float scanmax = 1.0f;
		ui.progressBar->setValue(8*scan_.normMin());

		// float scanmax = scan_.data_.maxCoeff();
		// ui.progressBar->setValue(scanmax);
		// scanmax *= 0.4f;

		QImage scan_img(rows, cols, QImage::Format_RGB32);
		int cx = scan_.getCenterCoord(servo_angle_);
		for(int y=0; y<cols; y++) {
			for(int x=0; x<rows; x++) {
				float q = scan_.at(x,y) / scanmax * 255.0f;
				if(q > 255.0f) q = 255.0f;
				unsigned char g = 255 - static_cast<int>(q);
				scan_img.setPixel(x, y, qRgb(g,g,g));
				// if(x == cx) {
				// 	scan_img.setPixel(x, y, qRgb(255,0,0));
				// }
				// if(y == 0) {
				// 	g = static_cast<int>(255.0f * scan_.norm_[x / scan_.NORM_SAMPLES] / scan_.norm_.maxCoeff());
				// 	scan_img.setPixel(x, y, qRgb(g,0,255-g));
				// }
			}
		}
		scan_image_ = scan_img;
		ui.label_scan->setPixmap(QPixmap::fromImage(scan_img));
	}

}
